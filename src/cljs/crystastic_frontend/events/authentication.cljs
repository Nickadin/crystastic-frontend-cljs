(ns crystastic-frontend.events.authentication
  (:require [re-frame.core :as re-frame]))

(defn authenticated [{username :username password :password}]
  (and (= username "Nick")
       (= password "test")))

; todo
(re-frame/reg-event-fx
  :authenticate-user
  (fn [coeffects [_ payload]]
    (let [db (:db coeffects)]
      (if (= (authenticated payload) true)
            {:db (assoc-in db [:user :token] "123")}
            {:db db}))))
