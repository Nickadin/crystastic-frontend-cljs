(ns crystastic-frontend.views
  (:require [re-frame.core :as re-frame]
            [crystastic-frontend.modules.authentication.containers.authenticated
              :as authentication-view]))

(defn main-panel []
  [:div
    [authentication-view/container
      [:div {:key 1} "abc"]]])
