(ns crystastic-frontend.events
  (:require [re-frame.core :as re-frame]
            [crystastic-frontend.db :as db]
            [crystastic-frontend.events.authentication]))

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   db/default-db))
