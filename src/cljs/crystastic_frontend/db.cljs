(ns crystastic-frontend.db)

(def default-user-db
    {:token nil :id nil :email nil})

(def default-db
  {:user default-user-db})
