(ns crystastic-frontend.modules.authentication.containers.authenticated
  (:require [re-frame.core :as re-frame]
            [crystastic-frontend.subs.authentication :as authentication-subs]
            [crystastic-frontend.modules.authentication.components.sign-in-form
              :as sign-in-form]))

(defn container [& children]
  (let [signed-in (re-frame/subscribe [::authentication-subs/signed-in])]
    [:div
      [:h1 "Hello authentication"]
      (if (= @signed-in true)
          children
          [sign-in-form/component
            {:login #(re-frame/dispatch [:authenticate-user %])}])]))
