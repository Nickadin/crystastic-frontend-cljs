(ns crystastic-frontend.modules.authentication.components.sign-in-form
  (:require [re-frame.core :as re-frame]))

(defn from-event [e] (-> e .-target .-value))

(defn field [props]
  [:div
    [:label (:name props)]
    [:input props]])

(defn component [{login-fn :login}]
  (let [state (atom {:username "" :password ""})
        save #(login-fn @state)]
    (fn []
      [:div
        [:h2 "Sign in"]
        [:form
          [field {:name "username"
                  :on-change #(swap! state assoc :username (from-event %))}]
          [field {:name "password" :type "password"
                  :on-change #(swap! state assoc :password (from-event %))}]
          [:button {:type "button" :on-click save} "Log in"]]])))
