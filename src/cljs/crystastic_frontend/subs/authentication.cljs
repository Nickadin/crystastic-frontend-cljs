(ns crystastic-frontend.subs.authentication
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::signed-in
  (fn [db]
    (-> db
        :user
        :token
        (boolean))))

